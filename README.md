# h1-windows-iso

This repository contains materials on building a special installation ISO for Windows. The ISO is used by the tools available in repository ```hyperonecom/h1-images-recommended``` to build Windows.

Building a Windows image requires the preparation of special installator ISO, which includes ```Autounattend.xml```:

* Installator installs Windows OS
* Script for installing all updates is run
* Additional scripts installing ie. GCE Windows Agent are run
  * GCE Windows Agent use KMS to activate Windows server
* Sysprep is run and server is shutdown

## Image features

* Installed [GCEWindowsAgent](https://github.com/hyperonecom/compute-image-windows)

## Autounattend.xml

```Autounattend.xml``` can be found in ```Autounattend```. It references scripts which can be found in ```scripts```.

To prepare new ISO use any software for editing ISO and place proper `Autounattend.xml` file in root of the standard MS Windows Installator ISO.

*WARNING*: Files is repo are named to know for which distro they are, ie. `Autounattend-Datacenter-Core.xml`, but file on ISO has to be named `Autounattend.xml` only.

## KMS - Windows Activation Server

To not place Activation Key in ```Autounattend.xml``` and be compliant with `GCEWindowsAgent` we use KMS activation. It uses well known keys against KMS activation server to activate instance OS. [More about KMS Activation](https://technet.microsoft.com/pl-pl/library/ff793419.aspx)

KMS server for hyperone.com is *kms.windows.hyperone.cloud* and is deployed on *RBX-KMS-10* server in ***WDC Infrastructure*** tenant. Server has only standard installation o KMS and firewall configured.

### Activating client

```cmd
slmgr.vbs -ipk ACTIVATION-KEY
slmgr.vbs /ato
slmgr.vbs /dli
```

### Troubleshooting

Best way to ts problems with activation is looking at logs. [Here](https://technet.microsoft.com/en-us/library/ee939272.aspx) is detailed information about logs structure

On client/server it is useful to see licence info:

```cmd
slmgr.vbs /dli
slmgr.vbs /dlv
```